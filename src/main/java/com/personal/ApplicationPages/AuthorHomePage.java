package com.personal.ApplicationPages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.personal.CommonToAllPages.BasePage;
import com.personal.CommonToAllPages.Browser;

public class AuthorHomePage extends BasePage{


    @FindBy(how = How.XPATH, using = "//a[contains(text(),'view all')]")
    List<WebElement> listViewAll;
    @FindBy(how = How.XPATH, using = "//a[contains(text(),'view less')]")
    WebElement linkViewLess;
    @FindBy(how = How.XPATH, using = "//a//span[contains(text(),'Start New Submission')]")
    WebElement btnStartNewSubmission;
    @FindBy(how = How.XPATH, using = "//a//span[contains(text(),'Accept')]")
    private WebElement btnAcceptInvitationFirst;
    @FindBy(how = How.XPATH, using = "//a//span[contains(text(),'OK')]")
    private WebElement btnOKAcceptInvitation;
    @FindBy(how = How.XPATH, using = "//button[contains(text(),'Confirm')]")
    private WebElement btnConfirmAgreeToRevise;
    @FindBy(how = How.XPATH, using = "//td//span[contains(text(),'Invited')]/following::a//span[contains(text(),'Accept')]")
    private WebElement btnAcceptInvitedStatus;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Refresh page")
    private WebElement linkRefreshPageInvitedStatus;
    @FindBy(how = How.XPATH, using = "//td[contains(text(),'My Incomplete Submissions')]/following::a[contains(text(),'view all')]")
    private WebElement linkViewAllMyIncompleteSubmissions;
    @FindBy(how = How.XPATH, using = "//td[contains(text(),'My Submissions that need Revisions')]/following::a[contains(text(),'view all')]")
    private WebElement linkViewAllMySubmissionNeedRevision;
    @FindBy(how = How.XPATH, using = "//td[contains(text(),'My Incomplete Submissions')]/following::a[contains(text(),'view all')]")
    private WebElement linkViewAllMyIncompleteSubmission;


    @FindBy(how=How.ID,using="pt1:pt_r1:0:signOut")
    private  WebElement linkLogOut;

    @FindBy(how=How.XPATH,using="//button[contains(text(),'OK')]")
    private WebElement btnOK;

    @FindBy(how=How.XPATH,using="//div[1]/div/div[1]/div/form/div/button/span/span[1]/span")
    private WebElement btnSignIn;

    public AuthorHomePage(Browser browser) {
        super(browser);

    }


    @Override
    protected WebElement elementForStartUp() {
        return btnStartNewSubmission;
    }

    public AuthorHomePage clickStartSubmission() {
        getBrowser().Click(btnStartNewSubmission);
        return this;
    }

    public AuthorHomePage logOut() throws InterruptedException{
        //wait.until(ExpectedConditions.elementToBeClickable(linkLogOut));
        Thread.sleep(5000);
        Click(linkLogOut);
        WaitForClickable(btnOK);
        Click(btnOK);
        WaitFor(btnSignIn);
        return  this;
    }
	
}
