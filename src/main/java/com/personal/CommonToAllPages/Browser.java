package com.personal.CommonToAllPages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.personal.Input.ConfigReader;

public class Browser {

	 private static final int DEFAULT_WAIT_SECONDS = 10;
	    WebDriver webDriver;
	    private ConfigReader configReader;

	    public Browser(WebDriver webDriver, ConfigReader configReader) {
	        this.webDriver = webDriver;
	        this.configReader = configReader;
	    }

	    void clickElement(WebElement element) {
	        element.click();
	    }

	    public void Click(WebElement element) {
	        waitUntil(element,DEFAULT_WAIT_SECONDS);
	        element.click();
	    }

	    public String currentUrl() {
	        return webDriver.getCurrentUrl();
	    }

	    public void load(String url, WebElement element) {
	        webDriver.get(url);
	        if (element != null) {
	            waitUntil(element, DEFAULT_WAIT_SECONDS);
	        }
	    }

	    public void load(WebElement element) {
	        if (element != null) {
	            waitUntil(element, DEFAULT_WAIT_SECONDS);
	        }
	    }

	    public void SendKeys(WebElement element,String text) {
	        waitUntil(element);
	        element.sendKeys(text);
	    }


	    public WebElement hasElement(WebElement element) {
	        return waitUntil(element, DEFAULT_WAIT_SECONDS);
	    }

	    public WebElement waitUntil(WebElement element, int timeOutInSeconds) {
	        try {
	            System.out.println("********************" +element);
	            return new WebDriverWait(webDriver, timeOutInSeconds)
	                    .ignoring(StaleElementReferenceException.class)
	                    .until(ExpectedConditions.visibilityOf(element));
	        } catch (TimeoutException timeoutException) {
	            throw new RuntimeException(timeoutException);
	        }
	    }

	    public WebElement waitUntil(WebElement element) {
	        try {
	            return new WebDriverWait(webDriver, DEFAULT_WAIT_SECONDS)
	                    .ignoring(StaleElementReferenceException.class)
	                    .until(ExpectedConditions.visibilityOf(element));
	        } catch (TimeoutException timeoutException) {
	            throw new RuntimeException(timeoutException);
	        }
	    }

	    public void waitUntilInvisible(WebElement element) {
	        try {
	             new WebDriverWait(webDriver, DEFAULT_WAIT_SECONDS)
	                    .ignoring(StaleElementReferenceException.class)
	                    .until(ExpectedConditions.invisibilityOf(element));
	        } catch (TimeoutException timeoutException) {
	            throw new RuntimeException(timeoutException);
	        }
	    }

	    public void waitUntilInvisible(WebElement element,int time) {
	        try {
	            new WebDriverWait(webDriver, time)
	                    .ignoring(StaleElementReferenceException.class)
	                    .until(ExpectedConditions.invisibilityOf(element));
	        } catch (TimeoutException timeoutException) {
	            throw new RuntimeException(timeoutException);
	        }
	    }

	    public WebElement waitUntilClickable(WebElement element,int time){
	        try {
	            return new WebDriverWait(webDriver, time)
	                    .ignoring(StaleElementReferenceException.class)
	                    .until(ExpectedConditions.elementToBeClickable(element));
	        } catch (TimeoutException timeoutException) {
	            throw new RuntimeException(timeoutException);
	        }
	    }

	    public WebElement waitUntilClickable(WebElement element){
	        try {
	            return new WebDriverWait(webDriver, DEFAULT_WAIT_SECONDS)
	                    .ignoring(StaleElementReferenceException.class)
	                    .until(ExpectedConditions.elementToBeClickable(element));
	        } catch (TimeoutException timeoutException) {
	            throw new RuntimeException(timeoutException);
	        }
	    }

	    public WebDriver getDriver() {
	        return webDriver;
	    }

	    public void closeDriver() {
	        webDriver.close();
	    }

	    public ConfigReader getConfigReader() {
	        return this.configReader;
	    }

	    public void openNewTab() {
	        Actions newTab = new Actions(webDriver);
	        newTab.sendKeys(Keys.CONTROL + "t").perform();
	    }


	
	
}
