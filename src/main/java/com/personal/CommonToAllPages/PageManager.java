package com.personal.CommonToAllPages;

import java.util.HashMap;
import java.util.Map;

import com.personal.Driver.DriverFactory;
import com.personal.Input.ConfigReader;

public class PageManager {

	private static ConfigReader configReader;
    private Browser browser;
    private Map<String, BasePage> pageMap = new HashMap<>();

    static {
        try {
            configReader = new ConfigReader("config.properties");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public PageManager() {
        browser = new Browser(DriverFactory.getDriver(), configReader);
    }

    public  <T extends BasePage> T navigate(Class<T> pageClass) throws Exception {
        T page;
        if(pageMap.containsKey(pageClass.getName())) {
           page = (T)pageMap.get(pageClass.getName());
        } else {
           page = pageClass.getDeclaredConstructor(Browser.class).newInstance(browser);
           pageMap.put(pageClass.getName(),page);
        }
        page.load();
        return page;
    }

    public void closeDriver(){
        browser.closeDriver();
    }

    public PageManager back(){
        browser.getDriver().navigate().back();
        return this;
    }
	
}
