package com.personal.CommonToAllPages;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public abstract class BasePage {

	private final Browser browser;
    private static BasePage instance;
    private PageManager pageManager;
    String url=null;

    @FindBy(how = How.ID, using = "//a[@title='Log Out']")
    private WebElement linkLogOut;

    BasePage(String baseUrl,String env, String journal, Browser browser) {
        url= MessageFormat.format(baseUrl,env,journal);
        PageFactory.initElements(new AjaxElementLocatorFactory(browser.getDriver(), 60), this);
        this.browser = browser;

    }

    public BasePage(Browser browser) {
        PageFactory.initElements(new AjaxElementLocatorFactory(browser.getDriver(), 60), this);
        this.browser = browser;
        //pageManager = new PageManager();
    }

    public <T extends BasePage> T load() {
        WebElement elementForstartup = elementForStartUp();
        if(url==null)
            this.browser.load(elementForstartup);
        else
            this.browser.load(url, elementForstartup);
        return (T)this;
    }

   /* public  <T extends BasePage> T navigate(Class<T> pageClass) throws Exception {
        return pageManager.navigate(pageClass);
    }*/
   private Map<String, BasePage> pageMap = new HashMap<>();
    public  <T extends BasePage> T navigate(Class<T> pageClass) throws Exception {
        T page;
        if(pageMap.containsKey(pageClass.getName())) {
            page = (T)pageMap.get(pageClass.getName());
        } else {
            page = pageClass.getDeclaredConstructor(Browser.class).newInstance(browser);
            pageMap.put(pageClass.getName(),page);
        }
        page.load();
        return page;
    }

    public boolean pageLoaded() {
        return this.browser.hasElement(elementForStartUp()) != null;
    }

    public Browser getBrowser() {
        return browser;
    }

    protected abstract WebElement elementForStartUp();

    public void ClickAllViewAllandWaitFor(List<WebElement> e, WebElement e1) {
        for(int i=0;i<e.size();i++) {
            if(e.get(i).isDisplayed()) {
                e.get(i).click();
                WaitFor(e1);
            }
        }
    }

    public void waitForAllToVisible(List<WebElement> e) {
        for(int i=0;i<e.size();i++) {
               WaitFor(e.get(i),600);
        }
    }

    public void waitForAllToVisible(List<WebElement> e,int time) {
        for(int i=0;i<e.size();i++) {
            WaitFor(e.get(i),time);
        }
    }

    public void waitForAllToInvisible(List<WebElement> e) {
        for(int i=0;i<e.size();i++) {
            WaitForInvisibility(e.get(i));
        }
    }

    public void waitForAllToInvisible(List<WebElement> e,int time) {
        for(int i=0;i<e.size();i++) {
            WaitForInvisibility(e.get(i),time);
        }
    }


    public void Click(WebElement element) {
        getBrowser().waitUntil(element);
        element.click();
    }

    public void SendKeys(WebElement element,String text) {
        getBrowser().waitUntil(element);
        element.sendKeys(text);
    }

    public void WaitFor(WebElement element) {
        getBrowser().waitUntil(element);

    }

    public void WaitFor(WebElement element,int time) {
        getBrowser().waitUntil(element,time);

    }

    public void WaitForInvisibility(WebElement element) {
        getBrowser().waitUntilInvisible(element);

    }

    public void WaitForInvisibility(WebElement element,int time) {
        getBrowser().waitUntilInvisible(element,time);

    }

    public void WaitForClickable(WebElement element,int time){
        getBrowser().waitUntilClickable(element,time);
    }

    public void WaitForClickable(WebElement element){
        getBrowser().waitUntilClickable(element);
    }

    public static BasePage getInstance(Browser browser) {

        if( instance == null ) {
            return null;
        }
        return instance;
    }
	
}
