package com.personal.CommonToAllPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class LogInPage extends BasePage{


    @FindBy(how = How.XPATH, using = "//input[@name='username']")
    private WebElement txtUserName;

    @FindBy(how = How.NAME, using = "password")
    private WebElement txtPassword;

    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Sign in')]")
    private WebElement btnSignIn;

    @FindBy(xpath = "//span[contains(text(),'New Submissions')")
    private WebElement titleNewSubmission;

    @FindBy(how = How.XPATH, using = "//a[@title='Log Out']")
    private WebElement linkLogOut;

    LogInPage(Browser browser) {
        super(browser.getConfigReader().getProperty("loginUrl"), browser.getConfigReader().getProperty("environment"),
                browser.getConfigReader().getProperty("journal"), browser);
    }

    @Override
    protected WebElement elementForStartUp() {
        return btnSignIn;
    }

    public LogInPage logIn(String emailId, String password) {
    	SendKeys(txtUserName,emailId);
    	SendKeys(txtPassword,password);
        btnSignIn.click();      
        return this;
    }

	
}
