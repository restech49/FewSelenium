package com.personal.Driver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Optional;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.personal.Input.ConfigReader;

import io.github.bonigarcia.wdm.Architecture;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.EdgeDriverManager;
import io.github.bonigarcia.wdm.InternetExplorerDriverManager;
import io.github.bonigarcia.wdm.MarionetteDriverManager;
import io.github.bonigarcia.wdm.OperaDriverManager;

public class DriverFactory {

	  /**
     * Is test run locally or on Jenkins
     */
    public static boolean localRun = false;
    private static WebDriver driver = null;

    /**
     * Empty constructor.
     */
    private DriverFactory() {
    }

    /**
     * Creates the webdriver after reading the specific browser type from the config
     * file on line 5. The newest executables are retrieved from a GitHub project
     * that keeps track of them.
     *
     * @return Returns the webdriver.
     * @throws MalformedURLException
     */
    public static WebDriver getDriver() {
        Optional<String> driverChoiceOptional = Optional.ofNullable(System.getenv("TEST_BROWSER_TYPE"));
        Optional<String> seleniumHubUrlOptional = Optional.ofNullable(System.getenv("SELENIUM_GRID_URL"));

        String driverChoice = driverChoiceOptional.orElseGet(() -> {
            localRun = true;
            ConfigReader reader = new ConfigReader("config.properties");
            return reader.getProperty("driver");
        });

        String seleniumHubUrl = seleniumHubUrlOptional.orElse("");

        if (localRun) {
            return getLocalWebDriver(driverChoice);
        }
        return getRemoteWebDriver(driverChoice, seleniumHubUrl);
    }

    /**
     * Returns a local WebDriver instance.
     *
     * @param driverChoice type of driver, e.g Chrome, Firefox, etc
     * @return
     */
    private static WebDriver getLocalWebDriver(String driverChoice) {
        switch (driverChoice) {
            case "Chrome":
                ChromeDriverManager.getInstance().setup();
                driver = new ChromeDriver();
                break;
            case "Firefox":
                MarionetteDriverManager.getInstance().setup();
                driver = new FirefoxDriver();
                break;
            case "IE":
                InternetExplorerDriverManager.getInstance().setup(Architecture.x32);
                driver = new InternetExplorerDriver();
                break;
            case "Edge":
                EdgeDriverManager.getInstance().setup();
                driver = new EdgeDriver();
                break;
            case "Opera":
                OperaDriverManager.getInstance().setup();
                driver = new OperaDriver();
                break;
            default:
                throw new IllegalArgumentException(
                        "Invalid driver. Choose between " + "Chrome, " + "Firefox, " + "IE, " + "Edge " + "or Opera");
        }
        return driver;
    }

    private static WebDriver getRemoteWebDriver(String driverChoice, String seleniumHubUrl) {
        DesiredCapabilities cap = DesiredCapabilities.firefox();
        switch (driverChoice) {
            case "Chrome":
                cap = DesiredCapabilities.chrome();
                break;
            case "Firefox":
                break;
            case "IE":
                cap = DesiredCapabilities.internetExplorer();
                cap.setVersion("ANY");
                cap.setPlatform(Platform.ANY);
                break;
            default:
                throw new IllegalArgumentException(
                        "Invalid driver. Choose between " + "Chrome, " + "Firefox, " + "or IE");
        }
        try {
            driver = new RemoteWebDriver(new URL(seleniumHubUrl), cap);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }

        return driver;
    }


	
}
