package com.personal.Input;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Set;

public class ConfigReader {

	private Properties prop;

	
	public ConfigReader(String propertyFile){
		
		try {
		File file = new File(propertyFile);
		FileInputStream fileInput = new FileInputStream(file);
		prop = new Properties();
		prop.load(fileInput);
		fileInput.close();
		}
		catch(Exception e) {
			System.out.println("Error to read file");
			e.printStackTrace();
		}
	}
	
	public Set<Object> getAllKeys(){
		Set<Object> keys = prop.keySet();
		return keys;
	}
	
	public String getPropertyValue(String key){
		return this.prop.getProperty(key);
	}

	public String getProperty(final String propName) {
		return prop.getProperty(propName);
	}

	public String getReportConfigPath(){
		String reportConfigPath = prop.getProperty("reportConfigPath");
		if(reportConfigPath!= null) return reportConfigPath;
		else throw new RuntimeException("Report Config Path not specified in the Configuration.properties file for the Key:reportConfigPath");		
	}
}
