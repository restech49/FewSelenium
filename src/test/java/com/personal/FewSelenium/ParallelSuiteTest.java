package com.personal.FewSelenium;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.personal.ApplicationPages.AuthorHomePage;
import com.personal.CommonToAllPages.LogInPage;
import com.personal.CommonToAllPages.PageManager;
import com.personal.Input.ConfigReader;

public class ParallelSuiteTest {

	PageManager pageManager;
	ConfigReader configReader;
	
	
    @BeforeTest
    public void beforeTest(){
    	configReader=new ConfigReader("config.properties");
    	pageManager=new PageManager();
    }
 
    @Test(dependsOnMethods= {"testMethodTwo"})
    public void testMethodOne() throws Exception {
    	
    	pageManager.navigate(LogInPage.class)
    		.logIn("auto.author49@mailinator.com", "Evise@2018");
    		    	
    }
    
    @Test
    public void testMethodTwo() throws Exception {
    	
    	pageManager.navigate(LogInPage.class)
    		.logIn("auto.author49@mailinator.com", "Evise@2018")
    		.navigate(AuthorHomePage.class)
    		.clickStartSubmission()
    		.logOut();
    		    	
    }
    
    @AfterTest
    public void tearOff() {
    	pageManager.closeDriver();
    }
 	
}
